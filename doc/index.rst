.. Hein Experiment Control documentation master file, created by
   sphinx-quickstart on Wed Apr 22 09:30:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hein Experiment Control's documentation!
===================================================

Hein Experiment control contains code for orchestrating and sequencing automation.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   hein_control


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
