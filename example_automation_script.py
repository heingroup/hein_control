"""
This is an example script on how to create an Automation
"""
import time
import logging
from hein_control.sequencing.automation import ConfiguredAutomation
from hein_control.action.configured import ConfiguredAction
from hein_control.action.logic import ConfiguredDecision

# logging is built-in to various aspects of the package. Logging can be enabled by applying a basic config
# (or otherwise configuring a logger) at the top of your script
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
)


"""
In an Automation, actions will be run in sequence. A decision making layer can also be added, 
to select what the next action to run is.
"""

# give functions the @ConfiguredAction decorator; when an Automation is run, TrackedActions will be created with
# specific configurations based on parameters passed into the Automation. These tracked actions will also contain
# additional data, such as the time they were started and completed
@ConfiguredAction
def action_1(arg):
    print('doing action 1')
    print(f'action 1 arg is: {arg}')
    time.sleep(1)
    return 'completed 1'


@ConfiguredAction
def action_2(arg):
    print('doing action 2')
    print(f'action 2 arg is: {arg}')
    time.sleep(1)
    print('next, decide to go to action 2 or action 3')
    return 'completed 2'


go_to_2 = True
@ConfiguredAction
def go_to_action_2() -> bool:
    """this action will be used to decide whether the next action should be 2 based"""
    global go_to_2
    if go_to_2 is True:
        print('go to action 2')
        go_to_2 = False
        return True
    else:
        return False


@ConfiguredAction
def go_to_action_3() -> bool:
    print('go to action 3')
    return True


@ConfiguredAction
def action_3():
    print('doing action 3')
    time.sleep(1)
    return 'completed 3'


# configure the decision making action
decision = ConfiguredDecision(go_to_action_2, go_to_action_3)

# set the sequence of actions
action_1.next_action = action_2
go_to_action_2.next_action = action_2
go_to_action_3.next_action = action_3
action_2.next_action = decision

# create the configured automation
automation_config = ConfiguredAutomation(first_action=action_1)

# create a tracked automation
automation = automation_config.get_tracked_from_config()

# run the automation with specific kew word arguments
kwargs = {
    'action_1': {'arg': 'action 1 arg'},
    'action_2': {'arg': 'action 2 arg'}
}
automation(**kwargs)
automation.wait_for_completion()
print('done')
executed_actions = automation.executed_actions
for action in executed_actions:
    print(f'{action.configuration.name}\n'
          f'\tuuid: {action.uuid}\n'
          f'\tstart time: {action.time_started}\n'
          f'\tend time: {action.time_completed}\n'
          f'\taction duration: {action.action_duration}\n'
          f'\taction return: {action.action_return}\n'
          )
