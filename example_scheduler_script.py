"""
This is an example script on the use of a Scheduler instance to trigger an action at predefined time points.

This example will perform the user_defined_action function once at 5 seconds, 5 times at 30 second intervals and
2 times at 60 second intervals
"""
import time
import logging
from hein_control import SamplingScheduler

# logging is built-in to various aspects of the package. Logging can be enabled by applying a basic config
# (or otherwise configuring a logger) at the top of your script
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
)


def user_defined_action():
    """Some user-defined action method which will perform the actions to be completed at every trigger"""
    print('Calling the slow "user_defined_action" function')
    # here the user would define the actions to be performed in sequence
    # in this example those actions take 10 seconds to complete
    time.sleep(10)
    print('The "user_defined_action" function is complete')


sched = SamplingScheduler(
    # actions are provided in order of execution
    # note that the action functions are provided as functions and not called
    # ("user_defined_action" not "user_defined_action()")
    user_defined_action
)

# time deltas may be provided during instantiation, but can also be defined manually

# single time points may be added individually
sched.insert_time_point(
    time_delta=5,  # seconds
)

# a list of time points can be added
# note that since we have added a time point already and we want these triggers to happen at 30 s, 60 s, etc. we need to
#   specify that the times are relative to the start
sched.insert_time_point_list(
    *[30, 60, 90, 120, 180],  # 5 times 30 second intervals
    position=-1,  # position as an index may be specified (see method documentation)
    relative_to='start',  # times are relative to the start
)

# multiple time points with the same spacing may also be added
sched.insert_time_point_list_pairs(
    [
        2,  # trigger two times
        60  # at 60 second intervals
    ],
)

# the time point list can be viewed for verification
print(sched.time_points)

# everything looks good... now we can start it

sched.start_sequence()

# if you're running (not executing in console), add a .join() call to wait for all points to complete
sched.join()
# tada!

print('everthing is all done!')
